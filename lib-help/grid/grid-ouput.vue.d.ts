declare const _default: import("vue").DefineComponent<{
    reMeta: ObjectConstructor;
}, {
    tableMeta: {};
    jsonTable: import("vue").ComputedRef<string>;
    json: import("vue").ComputedRef<string>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    reMeta: ObjectConstructor;
}>>, {}>;
export default _default;
