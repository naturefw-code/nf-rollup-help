declare const _default: import("vue").DefineComponent<{
    gridMeta: ObjectConstructor;
}, {
    reMeta: {
        gridMeta: {};
        gridTable: {};
        itemMeta: {};
    };
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    gridMeta: ObjectConstructor;
}>>, {}>;
/**
 * 直接维护 json
 */
export default _default;
