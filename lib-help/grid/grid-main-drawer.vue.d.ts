declare const _default: import("vue").DefineComponent<{
    gridMeta: ObjectConstructor;
    drawerInfo: ObjectConstructor;
}, {
    tabComp: {
        col: string;
        output: string;
        base: string;
    };
    reMeta: {};
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    gridMeta: ObjectConstructor;
    drawerInfo: ObjectConstructor;
}>>, {}>;
/**
 * 使用抽屉的方式，维护 json
 */
export default _default;
