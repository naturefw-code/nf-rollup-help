
// 扩展表单子控件
import { formItemKey } from './map'

import optionGroup from './extend/form/o-option-group.vue'
import optionTree from './extend/form/o-option-tree.vue'
import option from './extend/form/o-option.vue'

formItemKey[170] = option
formItemKey[171] = optionGroup
formItemKey[172] = optionTree

// 表单子控件
import helpItemCard from './base-item/base-card.vue'
import helpItemDrawer from './base-item/base-drawer.vue'
import helpItemExtend from './base-item/item-extend.vue'

// 表单子控件的扩展属性
// 文本类
import extendText from './base-item/extend/extend-text.vue'
// 数字类
import extendNumber from './base-item/extend/extend-number.vue'
// 日期类
import extendDate from './base-item/extend/extend-date.vue'
// 时间类
import extendTime from './base-item/extend/extend-time.vue'
// 上传类
import extendFile from './base-item/extend/extend-file.vue'
// 选择类
import extendChecks from './base-item/extend/extend-checks.vue'
// 下拉类
import extendSelect from './base-item/extend/extend-select.vue'

// 表单
import helpFormCard2 from './form/form.vue'
import helpFormCard from './form/form-main-card.vue'

// 列表
import helpGridCard from './grid/grid-main-card.vue'
import helpGridDrawer from './grid/grid-main-drawer.vue'
import helpGridColumn from './grid/grid-col.vue'

const state = {}

export {
  state,
  extendText,
  extendNumber,
  extendDate,
  extendTime,
  extendFile,
  extendChecks,
  extendSelect,

  // 子控件
  helpItemCard,
  helpItemDrawer,
  helpItemExtend,
  // 表单
  helpFormCard,
  helpFormCard2,
  // 列表
  helpGridColumn,
  helpGridDrawer,
  helpGridCard
}