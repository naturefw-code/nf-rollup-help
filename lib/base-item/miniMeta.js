import { reactive, watch } from 'vue'
import { typeName } from '@naturefw/nf-tool'
import { state } from '../main'

/**
 * 判断各种空值、数组、对象
 * @param {*} val 
 * @returns 
 */
const _isEmpty = (val) => {
  switch (typeName(val)) {
    case 'null':
    case 'undefined':
      return true
      break
    case 'string':
      return val.length === 0
      break
    case 'number':
    case 'bigInt':
      return isNaN(val)
      break
    case 'array':
      return val.length === 0
      break
    case 'object': 
      return Object.keys(val).length === 0
      break
    default:
      return false
      break
  }
}

/**
 * 不精简的属性
 */
const teshu = [
  'controls-position',
  'clearable',
  'validate-event',
  'show-word-limit',
  'show-password'
]

// 创建 字段的默认值
/**
 * 创建属性的默认值
 * @param itemMeta 配置文件
 * @returns 
 */
export const createColDefValue = (itemMeta) => {
  const re = {}
  for (const key in itemMeta) {
    const meta = itemMeta[key]
    re[meta.colName] = meta.defValue
  }
  return re
}


/**
 * 创建精简的 meta
 * @param {*} extendMeta  扩展属性
 * @param {*} baseDefaultValue 属性的默认值
 */
export const creatMiniMeta_extend = (extendMeta, baseDefaultValue) => {
  const miniMeta = state.outputItem.extendMeta
   // 遍历 extendMeta
  for (const key in extendMeta) {
    // 先判断类型，对象不能判断是否相等
    const m = extendMeta[key]
    if (_isEmpty(m)) {
      if (teshu.includes(key)) {
        // 同步属性值
        miniMeta[key] = m
      } else {
        // 没有设置，删除
        delete miniMeta[key]
      }
    } else {
      // 同步属性值
      miniMeta[key] = m

      if (m === baseDefaultValue[key]) {
        if (!teshu.includes(key)) {
          // 和默认值相等，去掉属性
          delete miniMeta[key]
        }
      }
    }
  }
}

export const extendSetup = (props, itemMeta) => {
  const defValue = createColDefValue(itemMeta)

  const _meta = reactive({}) // 基础属性的 meta
  const _partMeta = reactive({}) // 基础属性的 meta

  // 同步控件类型
  watch(() => state.itemMeta.controlType, (type) => {
    if (type)
      if (_meta.controlType !== type)
        _meta.controlType = type
  }, {immediate: true})

  watch(() => _meta.controlType, (type) => {
    if (type)
      if (state.itemMeta.controlType !== type)
      state.itemMeta.controlType = type
  })

  // 生成精简属性
  watch(_partMeta, () => {
    creatMiniMeta_extend(_partMeta, defValue)
  })

  return {
    _meta,
    _partMeta
  }
}