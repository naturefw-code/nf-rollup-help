declare const _default: import("vue").DefineComponent<{
    meta: ObjectConstructor;
    reMeta: ObjectConstructor;
}, {
    baseGridProps: {
        moduleId: number;
        formId: number;
        formColCount: number;
        colOrder: number[];
        formColShow: {};
        ruleMeta: {};
        itemMeta: {
            190: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            200: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                    "collapse-tags": boolean;
                };
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
        };
    };
    baseGridTable: {
        moduleId: number;
        formId: number;
        formColCount: number;
        colOrder: number[];
        formColShow: {};
        ruleMeta: {};
        itemMeta: {
            201: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: string;
                    label: string;
                }[];
                colCount: number;
            };
            202: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: string;
                    label: string;
                }[];
                colCount: number;
            };
            203: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            204: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            205: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            206: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            207: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            208: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            209: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: string;
                    label: string;
                }[];
                colCount: number;
            };
            210: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            211: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            212: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                extend: {
                    placeholder: string;
                    title: string;
                };
                optionList: {
                    value: string;
                    label: string;
                }[];
                colCount: number;
            };
            221: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            222: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            231: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            241: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: number;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            242: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: null;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            260: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: string;
                optionList: {
                    value: string;
                    label: string;
                }[];
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
            261: {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                defValue: boolean;
                extend: {
                    placeholder: string;
                    title: string;
                };
                colCount: number;
            };
        };
    };
    reMeta: Record<string, any> | undefined;
    json: import("vue").ComputedRef<string>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    meta: ObjectConstructor;
    reMeta: ObjectConstructor;
}>>, {}>;
export default _default;
