declare const _default: import("vue").DefineComponent<{
    formMeta: ObjectConstructor[];
}, {
    gridDom: import("vue").Ref<null>;
    gridItemModel: import("vue").Ref<{}>;
    dataList: never[];
    events: {
        add: () => void;
    };
    dialogInfo: {
        isShow: boolean;
        state: string;
        width: string;
        title: string;
    };
    delCol: (id: number) => void;
    addNew: () => void;
    handleCurrentChange: (val: any) => void;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    formMeta: ObjectConstructor[];
}>>, {}>;
export default _default;
