import { createApp } from 'vue'
import App from './App.vue'

// UI库
import ElementPlus1 from 'element-plus'
// import 'element-plus/lib/theme-chalk/index.css'
// import 'dayjs/locale/zh-cn'
// import locale from 'element-plus/lib/locale/lang/zh-cn'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

// 实现拖拽的自定义指令 
import {
  // config,
  // AllFormItem,
  nfElementPlus,
  formItemKey
} from '@naturefw/ui-elp'

// formItemKey[200] = test // 增加

// 设置icon
import installIcon from './icon/index'

// 简易路由
import router from './router/index'

// 设置 indexedDB 
import mydb from './db/cus'

const app = createApp(App)

app.use(router) // 路由
  .use(mydb)
  .use(ElementPlus1, { locale: zhCn, size: 'small' }) // UI库
  .use(installIcon) // 注册全局图标
  .use(nfElementPlus) // 全局注册 , {formItem}
  .mount('#app')
