/**
 * 读写用户的个性化方案
 */

// 创建help
import { dbCreateHelp } from '@naturefw/storage'

// 访问后端
// import axios from 'axios'
// 访问状态
// import { store } from '@naturefw/nf-state'

// 设置数据库名称和版本
const db = {
  dbName: 'nf-customer-setting',
  ver: 1
}

/**
 * 用户的个性化方案
 */
export default async function createDBHelp (callback) {
  // 设置基础路径
  // config.baseUrl = url

  const help = dbCreateHelp({
    dbFlag: 'nf-customer-setting',
    dbConfig: db,
    stores: { 
      /**
       * * caseId：'方案编号',
       * * moduleId: '模块ID',
       * * label: '方案名称',
       * * meta: { // meta内容
       * * * header-align: '标题对齐方式'
       * * * align: '内容对齐方式'
       * * * width: '宽度'
       * * }
       */
      cus_grid: { // 列表的个性化方案
        id: 'caseId',
        index: {
          moduleId: false
        },
        isClear: false
      },
      /**
       * 属性：
       * caseId：'方案编号',
       * moduleId: '模块ID',
       * label: '方案名称',
       * meta: { // meta内容
       * }
       */
      cus_find: { // 查询
        id: 'caseId',
        index: {
          moduleId: false
        },
        isClear: false
      },
      /**
       * 属性：
       * caseId：'方案编号',
       * moduleId: '模块ID',
       * formId: '表单ID',
       * label: '方案名称',
       * meta: { // meta内容
       * }
       */
      cus_form: { // 表单
        id: 'caseId',
        index: {
          moduleId: false,
          formId: false
        },
        isClear: false
      }
    },
    // 设置初始数据
    async init (help) {
    }
  })
  return help
}