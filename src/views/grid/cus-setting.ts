
import { reactive, watch } from 'vue'
import { ElMessage } from 'element-plus'

// 创建help
import { useDBHelp, useStores } from '@naturefw/storage'
// indexedDB 的标识  
const dbFlag = 'nf-customer-setting'
    
/**
 * 管理个性化设置
 * * 获取方案：获取列表by模块id；获取某个方案bycaseID
 * * 添加方案：addNew
 */
    
export const help = useDBHelp(dbFlag)
export const { cus_grid } = useStores(dbFlag)

/**
 * 个性化方案 的 meta
 */
type ICase = {
  gridMeta: {
    colOrder: Array<number>
  },
  itemMeta: {
    [index: string | number]: {
      'header-align': string,
      'align': string,
      width: number | string
    }
  }
}

/**
 * 个性化方案
 */
type ICaseList = {
  caseId: string | number,
  moduleId: string | number,
  label: string,
  meta: ICase
}

/**
 * 创建 默认 的 方案
 * @param props 
 * @returns 
 */
const createDefalutCase = (props: any) => {
  const defaultCase: ICaseList = {
    caseId: '1',
    moduleId: 100,
    label: '默认',
    meta: {
      gridMeta: {
        colOrder: [...props.meta.gridMeta.colOrder]
      },
      itemMeta: {}
    }
  }
  
  const arr = Object.keys(props.meta.itemMeta)
  arr.forEach(id => {
    defaultCase.meta.itemMeta[id] = {
      width: props.meta.itemMeta[id].width,
      align: props.meta.itemMeta[id].align,
      'header-align': props.meta.itemMeta[id]['header-align']
    }
  })

  return defaultCase
}

/**
 * 设置 保存 和另存为按钮是否显示
 * @param caseList 方案列表
 * @param caseInfo 当前方案
 */
const setShow = (caseList: Array<ICaseList>, caseInfo: any) => {

  const ca = caseList.find((item: any) => item.caseId === caseInfo.caseId)
  
  if (caseInfo.caseId === '1') {
    // 默认
    caseInfo.showNew = caseInfo.isChange
    caseInfo.showSave = false
  } else {
    // 个性化
    if (caseInfo.caseName === ca.label) {
      // 名称相等，只能修改
      caseInfo.showNew = false
      caseInfo.showSave = caseInfo.isChange
    } else {
      // 名称不等，只能另存
      caseInfo.showNew = caseInfo.isChange
      caseInfo.showSave = false
    }
  }
}

// ============================= 对外 =============================

/**
 * 列表的个性化方案的管理类
 * @param props 组件的 props
 * @returns 
 */
export function gridCaseManage(props: any) {
  
  // 记录默认方案
  const defaultCase: ICaseList = createDefalutCase(props)

  // 个性化方案的列表
  const caseList = reactive<Array<ICaseList>>([])
  // 获取列表
  const getList = async () => {
    const arr = await cus_grid.list()
    caseList.length = 0
    caseList.push(defaultCase)
    caseList.push(...arr)
  }

  // 当前的方案的信息
  const caseInfo = reactive({
    caseId: '1', // 当前的方案ID
    caseName: '新方案',
    isChange: false, // meta 是否变更过
    byMeta: false, // 区分正常切换，还是调整meta
    showSave: false, // 是否显示 保存 按钮
    showNew: false // 是否显示 另保存 按钮
  })

  // 监听 meta 是否变更
  watch(props.meta, () => {
    if (caseInfo.byMeta) {
      caseInfo.isChange = true
    } else {
      caseInfo.byMeta = true
    }
    setShow (caseList, caseInfo) 

  })

  // 监听方案名称的变化
  watch(() => caseInfo.caseName, () => {
    setShow (caseList, caseInfo) 
  })

  // 监听方案选项的变化
  watch(() => caseInfo.caseId, (id) => {
    
    caseInfo.byMeta = false
    caseInfo.isChange = false // 切换方案，视为没变
    setShow (caseList, caseInfo)
    
    const ca = caseList.find((item) => item.caseId === id)
    if (ca) {
      // 设置名称
      caseInfo.caseName = ca.label
      // 设置 排序
      props.meta.gridMeta.colOrder.length = 0
      props.meta.gridMeta.colOrder.push(...ca.meta.gridMeta.colOrder)
      // 设置对齐
      const arr = Object.keys(props.meta.itemMeta)
      arr.forEach(id => {
        if (ca.meta.itemMeta[id]) {
          props.meta.itemMeta[id].width = ca.meta.itemMeta[id].width
          props.meta.itemMeta[id].align = ca.meta.itemMeta[id].align
          props.meta.itemMeta[id]['header-align'] = ca.meta.itemMeta[id]['header-align']
        }
      })
    }

    
  })

  /**
   * 添加方案
   */
  const addNewCase = () => {
  
    const case1: ICaseList = createDefalutCase(props)

    case1.caseId = new Date().valueOf()
    case1.label = caseInfo.caseName

    cus_grid.add(case1).then(() => {
      getList()
      caseInfo.isChange = false
      ElMessage({
        message: `恭喜你！成功添加了【${case1.label}】的个性化方案。`,
        type: 'success',
      })
    })
  }

  /**
   * 保存方案
   */
  const saveCase = () => {
    const ca = caseList.find((item) => item.caseId === caseInfo.caseId)
    if (ca) {
      
      const case1: ICaseList = createDefalutCase(props)

      case1.caseId = ca.caseId
      case1.moduleId = ca.moduleId
      case1.label = ca.label
       
      cus_grid.put(case1).then(() => {
        getList()
        caseInfo.isChange = false // 保存后，视为没变
        ElMessage({
          message: `恭喜你！成功修改了【${case1.label}】的个性化方案。`,
          type: 'success',
        })
      })
    }
  }

  /**
   * 删除当前的方案
   */
  const delCase = () => {
    cus_grid.del(caseInfo.caseId).then(() => {
      getList()
      ElMessage({
        message: `恭喜你！成功删除了【${caseInfo.caseName}】的个性化方案。`,
        type: 'success',
      })
    })
  }

  return {
    caseInfo,
    getList,
    caseList,
    // 操作
    addNewCase,
    saveCase,
    delCase
  }

}