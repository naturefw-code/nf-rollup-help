import { Document, FolderOpened } from '@element-plus/icons-vue'
import { createRouter } from '@naturefw/ui-elp'

import home from '../views/home.vue'

const url = import.meta.env.BASE_URL
const baseUrl = url === '/' ? '': '' + url

export default createRouter({
  /**
   * 基础路径
   */
  baseUrl: baseUrl,
  /**
   * 首页
   */
  home: home,

  menus: [
    {
      menuId: '3',
      title: '维护json',
      naviId: '0',
      path: 'help',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '3060',
          title: '列表控件(card)',
          path: 'grids',
          icon: Document,
          component: () => import('../views/grid/10-grid.vue')
        },
        {
          menuId: '3070',
          title: '列表控件(抽屉)',
          path: 'grid-drawer',
          icon: Document,
          component: () => import('../views/grid/20-grid-drawer.vue')
        },
        {
          menuId: '3010',
          title: '表单子控件(card)',
          path: 'item-card',
          icon: Document,
          component: () => import('../views/item/10-item-card.vue')
        },
        {
          menuId: '3012',
          title: '表单子控件(抽屉)',
          path: 'item-drawer',
          icon: Document,
          component: () => import('../views/item/20-item-drawer.vue')
        },
        {
          menuId: '3020',
          title: '表单控件(card)',
          path: 'form-card',
          icon: Document,
          component: () => import('../views/form/10-form.vue')
        },
        {
          menuId: '3022',
          title: '表单控件(抽屉)',
          path: 'form-drawer',
          icon: Document,
          component: () => import('../views/form/20-form-slot.vue')
        }
      ]
    } 
  ]
})
