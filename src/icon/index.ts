// import { reactive } from 'vue'

// import * as Icons from "@element-plus/icons-vue"

import {
  CloseBold,
  Close,
  Plus,
  Star,
  UserFilled,
  Loading,
  Connection,
  Edit,
  FolderOpened
} from '@element-plus/icons-vue'

const dictIcon = {
  'CloseBold': CloseBold,
  'Close': Close,
  'Plus': Plus,
  'Star': Star,
  'UserFilled': UserFilled,
  'Loading': Loading,
  'Connection': Connection,
  'Edit': Edit,
  'FolderOpened': FolderOpened
}

const installIcon = (app: any) => {
  for (const [key, component] of Object.entries(dictIcon)) {
    app.component(key, component)
  }
  // 便于模板获取
  // app.config.globalProperties.$icon = dictIcon
  // 使用全部图标
  // app.config.globalProperties.$icon = Icons
}

export default installIcon